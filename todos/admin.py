from django.contrib import admin
from todos.models import (
    TodoItem,
    TodoList,
)
class TodoListAdmin(admin.ModelAdmin):
    pass
class TodoItemAdmin(admin.ModelAdmin):
    pass
# Register your models here.

admin.site.register(TodoItem, TodoItemAdmin)
admin.site.register(TodoList, TodoListAdmin)
